# Giovanni van Donge, 6969763 (g.m.vandonge@students.uu.nl)
import os
import csv
import glob


def main():
    # Set output directories of the NetMHCpan-4.0 analysis and save files to lists
    path_ba = "/home/giovanni/Documents/major_internship_uu/netMHCpan_output/BindingAffinity/"
    path_el = "/home/giovanni/Documents/major_internship_uu/netMHCpan_output/ElutedLigand/"
    files_ba_mut, files_ba_wt, files_el_mut, files_el_wt = read_directory(path_ba, path_el)

    # Parse the NetMHCpan-4.0 output files
    ba_content = parse_content(path_ba, files_ba_mut, files_ba_wt, "BA")
    el_content = parse_content(path_el, files_el_mut, files_el_wt, "EL")

    # Write parsed content to output file
    write_to_file(ba_content, "BA_")
    write_to_file(el_content, "EL_")


def read_directory(path_ba, path_el):
    """
    Read files in given directories and saves them to sorted lists.
    :param path_ba: path of directory which contains the Binding Affinity output of NetMHCpan-4.0.
    :param path_el: path of directory which contains the Eluted Ligand output of NetMHCpan-4.0.
    :return: Mutant binding affinity file list, wildtype binding affinity file list, mutant eluted ligand file list, wildtype eluted ligand file list.
    """
    # Binding affinity directory
    os.chdir(path_ba)
    files_ba_mut = sorted(glob.glob('*_mutant.xls'))
    files_ba_wt = sorted(glob.glob('*_wildtype.xls'))
    # Eluted ligand directory
    os.chdir(path_el)
    files_el_mut = sorted(glob.glob('*_mutant.xls'))
    files_el_wt = sorted(glob.glob('*_wildtype.xls'))

    return files_ba_mut, files_ba_wt, files_el_mut, files_el_wt


def parse_content(path, mutant_files, wildtype_files, setting):
    """

    :param path: path of NetMHCpan-4.0 output directory.
    :param mutant_files: list of the NetMHCpan-4.0 mutant output files.
    :param wildtype_files: list of the NetMHCpan-4.0 wildtype output files.
    :param setting: which NetMHCpan-4.0 output is analysed: binding affinity or eluted ligand? (BA/EL)
    :return: 2D list of the parsed content.
    """
    # Set header of output file according to the input settings (BA/EL)
    if setting == "BA":
        out_2d_list = [["Study", "Mutant_peptide", "Normal_peptide", "HLA_allele", "Patient", "Mutant_affinityRank_4.0", "Normal_affinityRank_4.0",
                        "Mutant_affinity_4.0", "Normal_affinity_4.0"]]
    else:
        out_2d_list = [["Study", "Mutant_peptide", "Normal_peptide", "HLA_allele", "Patient", "Mutant_elutedligandRank_4.0", "Normal_elutedligandRank_4.0",
                        "Mutant_elutedligandScore_4.0", "Normal_elutedligandScore_4.0"]]

    # Parse content of mutant and wildtype files in given directory
    rank_index_list = []
    for x in range(len(mutant_files)):
        print("Parsing:", mutant_files[x], "and", wildtype_files[x])

        # Read both the mutant and wildtype files line by line
        with open(path + mutant_files[x], "r") as mut_file, open(path + wildtype_files[x], "r") as wt_file:
            for mut_line, wt_line in zip(mut_file, wt_file):

                # Save (i) HLA alleles, (ii) header and (iii) rank index positions in header. Only use the mutant file for this because these are the same
                # in both files.
                if mut_line.startswith('\t'):  # Save HLA alleles to list
                    hla = mut_line.strip().split('\t')
                    hla = [i for i in hla if i != ""]  # Remove all empty items from list
                elif mut_line.startswith('Pos'):  # Save header to list
                    header = mut_line.strip().split('\t')
                    rank_index_list = get_index_positions(header, 'Rank')

                else:  # For every line that is not the HLA line or header: save line to list
                    mut_line_list = mut_line.strip().split('\t')
                    wt_line_list = wt_line.strip().split('\t')

                    # Determine best ranks of the mutant peptides
                    best_mut_rank = 10000000
                    mut_rank_list = []
                    wt_rank_list = []
                    for i in rank_index_list:
                        # First, find the best rank
                        if float(mut_line_list[i]) < float(best_mut_rank):
                            best_mut_rank = mut_line_list[i]
                            best_wt_rank = wt_line_list[i]
                        # Add rank to best_rank_list if it is below threshold of 2%
                        if float(mut_line_list[i]) < 2:  # THRESHOLD
                            mut_rank_list.append(mut_line_list[i])
                            wt_rank_list.append(wt_line_list[i])

                    # Find corresponding score(s) of the best rank(s) and save all info to 2D list.
                    if len(mut_rank_list) == 0:  # If best rank list is empty, add lowest rank found in the line
                        mut_rank_list.append(best_mut_rank)
                        wt_rank_list.append(best_wt_rank)

                    # For each rank in the rank list, get the score and hla and save to 2D list.
                    # TODO: if length mut_rank_list > 1, zorg dat de extra regel niet de T cell response krijgt van de originele regel want die weten we helemaal
                    #  niet. Maar hoe weet je wat de originele regel is dan?? Misschien beter in R aanpassen??????
                    mut_rank_index = 0
                    for mut_rank, wt_rank in zip(mut_rank_list, wt_rank_list):
                        for y in rank_index_list:  # Cannot use list.index(rank) because the value for rank is not unique in the line!
                            if mut_line_list[y] == mut_rank:
                                mut_rank_index = y

                        mut_score = find_score(mut_rank_index, mut_line_list, setting)
                        wt_score = find_score(mut_rank_index, wt_line_list, setting)  # Use same rank index as mutant rank index
                        best_hla = find_hla(mut_rank_index, hla)
                        write_to_outlist(out_2d_list, mutant_files[x], best_hla, mut_line_list, mut_score, mut_rank, wt_line_list, wt_score, wt_rank)

    return out_2d_list


def get_index_positions(elements_list, element):
    """
    Returns the indexes of all occurrences of given element in the list.
    :param elements_list: list which will be used.
    :param element: item of which the indexes will be determined in the given list.
    :return: list of indexes.
    """
    index_pos_list = []
    index_pos = 0
    while True:
        try:
            # Search for item in list from indexPos to the end of list
            index_pos = elements_list.index(element, index_pos)
            # Add the index position in list
            index_pos_list.append(index_pos)
            index_pos += 1
        except ValueError as e:
            break

    return index_pos_list


def find_score(rank_index, line_list, setting):
    """
    Find the corresponding score of the given rank.
    :param rank_index: rank index of which the corresponding score will be determined.
    :param line_list: list of the line of the given rank.
    :param setting: BA/EL?
    :return: corresponding score.
    """
    if setting == "BA":
        score = line_list[rank_index - 1]
    else:
        score = line_list[rank_index - 2]

    return score


def find_hla(rank_index, hla):
    """
    Find the corresponding HLA of the given rank.
    :param rank_index: rank index.
    :param hla: list of HLA alles (originating from the input files).
    :return:
    """
    if rank_index - 7 == 0:
        best_hla_index = 0
    else:
        best_hla_index = int((rank_index - 7) / 5)
    best_hla = hla[best_hla_index]

    return best_hla


def write_to_outlist(out_2d_list, file, best_hla, mut_line, mut_score, mut_rank, wt_line, wt_score, wt_rank):
    """
    Writes given content to the 2D output list.
    :param out_2d_list: 2D output list.
    :param file: current file which is being read.
    :param best_hla: HLA allele that corresponds the best mutant rank.
    :param mut_line: list of the line in the mutation file.
    :param mut_score: mutant score that corresponds with the best mutant rank.
    :param mut_rank: best mutant rank.
    :param wt_line: list of the line in the wildtype file.
    :param wt_score: score that corresponds with the wildtype rank.
    :param wt_rank: rank that corresponds with the mutant rank.
    :return: no return, adds the content as a list to the 2D output list.
    """
    study = file.split("-")[0][3:]
    patient = " ".join(file.split("-")[1].split("_")[:-1])

    # Save all content to a list which is appended to the 2D output list.
    out_line = [study, mut_line[1], wt_line[1], best_hla, patient, mut_rank, wt_rank, mut_score, wt_score]
    out_2d_list.append(out_line)


def write_to_file(content, prefix):
    """
    Writes the parsed content in the 2D list to an output file.
    :param content: content in 2D list.
    :param prefix: BA_/EL_
    :return: no return, writes to file.
    """
    with open("../../netMHCpan_parsed/"+prefix+"parsed.csv", "w+") as my_csv:
        csv_writer = csv.writer(my_csv, delimiter=',')
        csv_writer.writerows(content)


main()
