# Giovanni van Donge, 6969763 (g.m.vandonge@students.uu.nl) ####
# Plot all the figures from Bjerregaard et al. with new NetMHCpan-4.0 output


# LIBRARIES -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
library(readr)
library(ggplot2)
library(ggbeeswarm)
library(scales)
library(plotROC)
library(ggfortify)


# IMPORT DATA ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
data.peptides <- read_csv("../netMHCpan_parsed/peptide_data.csv")
data.peptides <- data.peptides[complete.cases(data.peptides[,"Tcell_response"]),]  # Remove rows with unknown T cell response
data.subset.T <- data.peptides[ which(data.peptides$Tcell_response == 'YES'), ]
data.subset.No.T <- data.peptides[ which(data.peptides$Tcell_response == 'NO'), ]


# PLOT FIGURE 1A ------------------------------------------------------------------------------------------------------------------------------------------------------------------
plot.ELpercRank <- ggplot(data=data.peptides, aes(x=Study,y=Mutant_elutedligandRank_4.0,group=Tcell_response)) +
  ylab('Mutant peptide EL %Rank') +
  xlab('') +
  ggtitle("Predicted EL %RANK score of neopeptides") +
  scale_y_log10(breaks=c(.01,.1,.5,1,2,10,100),labels=c(.01,.1,.5,1,2,10,100)) +
  # Beeswarm peptides with NO T cell response
  geom_beeswarm(data=data.subset.No.T,aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
                cex=.5,show.legend=FALSE,alpha=.5) +
  # Beeswarm peptides with T cell response
  geom_beeswarm(data=data.subset.T,aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
                show.legend=FALSE,alpha=.6) +
  scale_shape_manual(values=c(16, 18)) +
  scale_color_manual(values=c('#FF9999','#56B4E9')) +
  scale_size_manual(values=c(2,3)) +
  geom_hline(yintercept=2, linetype="dashed", color = "#56B4E9") +
  geom_hline(yintercept=0.5, linetype="dashed", color = "#56B4E9") +
  theme_bw() +
  theme(axis.text.x=element_text(angle = -90, hjust = 0))
plot.ELpercRank


# PLOT FIGURE 1B ------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Mann-Whitney U test
tcell <- data.peptides[ which(data.peptides$Tcell_response=='YES'), ]$Mutant_elutedligandRank_4.0
no.tcell <- data.peptides[ which(data.peptides$Tcell_response=='NO'), ]$Mutant_elutedligandRank_4.0
wilcox_EL<- wilcox.test(tcell, no.tcell, alternative='two.sided')
wilcox_EL
pvalue.label <- paste("p = ", round(wilcox_EL$p.value,9), sep = "")
# Plot
plot.Tcell.response <- ggplot(data=data.peptides, aes(x=Tcell_response,y=Mutant_elutedligandRank_4.0,group=Tcell_response)) +
  ylab('Mutant peptide EL %Rank') +
  xlab('T cell response') +
  scale_y_log10(breaks=c(.01,.1,.5,1,2,10,100),labels=c(.01,.1,.5,1,2,10,100)) +
  geom_beeswarm(aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
                cex=.7,show.legend=FALSE,alpha=.5) +
  scale_shape_manual(values=c(16, 18)) +
  scale_color_manual(values=c('#FF9999','#56B4E9')) +
  scale_size_manual(values=c(2,3)) +
  geom_boxplot(outlier.shape = NA, alpha=0) +
  annotate("text", x=1.5, y=10, label=pvalue.label, size = 4) +
  geom_hline(yintercept=2, linetype="dashed", color = "#56B4E9") +
  geom_hline(yintercept=0.5, linetype="dashed", color = "#56B4E9") +
  theme_bw()
plot.Tcell.response


# PLOT FIGURE 1C ------------------------------------------------------------------------------------------------------------------------------------------------------------------
scatter.normal.mutant <- ggplot(data=data.peptides, aes(x=Mutant_elutedligandRank_4.0,y=Normal_elutedligandRank_4.0,group=Tcell_response)) +
  scale_y_log10(breaks=c(.01,.1,.5,1,2,10,100),labels=c(.01,.1,.5,1,2,10,100)) +
  scale_x_log10(breaks=c(.01,.1,.5,1,2,10,100),labels=c(.01,.1,.5,1,2,10,100)) +
  ylab('Normal peptide EL %Rank') +
  xlab('Mutant peptide EL %Rank') +
  geom_point(data=data.subset.No.T,aes(shape=Tcell_response,color=Tcell_response,size=DAI),alpha=0.2) +  # size=Anchor
  geom_point(data=data.subset.T,aes(shape=Tcell_response,color=Tcell_response,size=DAI),alpha=0.5) +  # size=Anchor
  scale_shape_manual(values=c(16, 18)) +
  geom_hline(yintercept=2, linetype="dashed", color = "#56B4E9") +
  geom_hline(yintercept=0.5, linetype="dashed", color = "#56B4E9") +
  geom_vline(xintercept=2, linetype="dashed", color = "#56B4E9") +
  geom_vline(xintercept=0.5, linetype="dashed", color = "#56B4E9") +
  theme_bw()
scatter.normal.mutant

data.peptides.tcell <- data.peptides[data.peptides$Tcell_response == "YES",]
scatter.normal.mutant <- ggplot(data=data.peptides.tcell, aes(x=Mutant_elutedligandRank_4.0,y=Normal_elutedligandRank_4.0,group=AminoAcid_change)) +
  geom_point(data=data.peptides.tcell,aes(color=AminoAcid_change,size=5),alpha=0.2) +
  xlim(0, 2.5) +
  ylim(0, 10)
scatter.normal.mutant

# PLOT FIGURE 2 -------------------------------------------------------------------------------------------------------------------------------------------------------------------
# TODO: Add self_similarity calculations to data.peptides in netMHCpan_merge.R
# TODO: Merge 3 seperate plots into one plot

# ALL
# t.test.all <- t.test(data.peptides[ which(data.peptides$Tcell_response=='NO'), ]$Self_similarity,
#                      data.peptides[ which(data.peptides$Tcell_response=='YES'), ]$Self_similarity, alternative = 'two.sided')
# t.test.all
# self.similarity.all <- ggplot(data=data.peptide, aes(x=Tcell_response,y=`Self-similarity`,group=Tcell_response)) +
#   ylab('Self-similarity') +
#   xlab('T cell response') +
#   ggtitle('All') +
#   geom_beeswarm(aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
#                 cex=.7,alpha=.5) +
#   scale_shape_manual(values=c(16, 18)) +
#   scale_color_manual(values=c('#FF9999','#56B4E9')) +
#   scale_size_manual(values=c(2,3)) +
#   geom_boxplot(aes(fill=Tcell_response),outlier.shape = NA, alpha=0.6) +
#   annotate(geom="text", x=1.5, y=0.990, label="ns") +
#   theme_bw()
# self.similarity.all

# TODO: Update this with right data and columns
# IMPROVED BINDING (IB) (ratio >= 1.2)
# data.subset.IB <- data.peptide[ which(data.peptide$EL_ratio>=1.2), ]
# t.test.IB <- t.test(data.subset.IB[ which(data.subset.IB$Tcell_response=='NO'), ]$`Self-similarity`,
#                   data.subset.IB[ which(data.subset.IB$Tcell_response=='YES'), ]$`Self-similarity`, alternative = 'two.sided')
# t.test.IB
# self.similarity.IB <- ggplot(data=data.subset.IB, aes(x=Tcell_response,y=`Self-similarity`,group=Tcell_response)) +
#   ylab('Self-similarity') +
#   xlab('T cell response') +
#   ggtitle('Improved binding') +
#   geom_beeswarm(aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
#                 cex=.7,alpha=.5) +
#   scale_shape_manual(values=c(16, 18)) +
#   scale_color_manual(values=c('#FF9999','#56B4E9')) +
#   scale_size_manual(values=c(2,3)) +
#   geom_boxplot(aes(fill=Tcell_response),outlier.shape = NA, alpha=0.6) +
#   annotate(geom="text", x=1.5, y=0.990, label="ns") +
#   theme_bw()
# self.similarity.IB

# CONSERVED BINDING (CB) (ratio < 1.2)
# data.subset.CB <- data.peptide[ which(data.peptide$EL_ratio<1.2), ]
# t.test.CB <- t.test(data.subset.CB[ which(data.subset.CB$Tcell_response=='NO'), ]$`Self-similarity`,
#                   data.subset.CB[ which(data.subset.CB$Tcell_response=='YES'), ]$`Self-similarity`, alternative = 'two.sided')
# t.test.CB
# self.similarity.CB <- ggplot(data=data.subset.CB, aes(x=Tcell_response,y=`Self-similarity`,group=Tcell_response)) +
#   ylab('Self-similarity') +
#   xlab('T cell response') +
#   ggtitle('Conserved binding') +
#   geom_beeswarm(aes(shape=Tcell_response,color=Tcell_response,size=Tcell_response),
#                 cex=.7,alpha=.5) +
#   scale_shape_manual(values=c(16, 18)) +
#   scale_color_manual(values=c('#FF9999','#56B4E9')) +
#   scale_size_manual(values=c(2,3)) +
#   geom_boxplot(aes(fill=Tcell_response),outlier.shape = NA, alpha=0.6) +
#   annotate(geom="text", x=1.5, y=0.990, label="*", size=10) +
#   theme_bw()
# self.similarity.CB

# TODO: Add boxplots to one figure
# USE FACETWRAP() -->> ggplot(longtest, aes(d = D, m = M)) + geom_roc() + facet_wrap(~ name) + style_roc()


# PLOT FIGURE 3 -------------------------------------------------------------------------------------------------------------------------------------------------------------------
# TODO: Update this with right data and columns
# TODO: Understand ROC, doesn't work yet, wrong data?

# Add 0/1 to dataframe for plotROC to use. Tcell_response: YES = 0, NO = 1
# data.peptides$D <- 1
# data.peptides$D[data.peptides$Tcell_response=='YES'] <- 0
# # Transform data for plotROC
# melt_data.peptides <- melt_roc(data.peptides, "D", c("Mutant_elutedligandRank_4.0", "DAI", "Self_similarity"))
# head(melt_data.peptides)
# # Plot ROC
# roc.all <- ggplot(melt_data.peptides, aes(d = D.D, m = M, color = name)) + 
#   scale_x_continuous(breaks=c(0,0.1,0.25,0.50,0.75,0.9,1),labels=c(0,0.1,0.25,0.50,0.75,0.9,1)) +
#   scale_y_continuous(breaks=c(0,0.1,0.25,0.50,0.75,0.9,1),labels=c(0,0.1,0.25,0.50,0.75,0.9,1)) +
#   geom_roc(n.cuts=0) +
#   ylab('True positive fraction') +
#   xlab('False positive fraction') +
#   ggtitle('All peptides')
# roc.all
# # Add area under curve (AUC)
# auc <- calc_auc(roc.all)
# roc.all + 
#   annotate("text", x = .75, y = .25, label = paste("AUC =", round(auc$AUC[1], 2)), color='blue') +
#   annotate("text", x = .75, y = .20, label = paste("AUC =", round(auc$AUC[2], 2)), color='green') +
#   annotate("text", x = .75, y = .15, label = paste("AUC =", round(auc$AUC[3], 2)), color='red')

# data.subset.IB <- data.peptide[ which(data.peptide$EL_ratio>=1.2), ]
# data.subset.CB <- data.peptide[ which(data.peptide$EL_ratio<1.2), ]


## EXAMPLE
# set.seed(2529)
# D.ex <- rbinom(200, size = 1, prob = .5)
# M1 <- rnorm(200, mean = D.ex, sd = .65)
# M2 <- rnorm(200, mean = D.ex, sd = 1.5)
# test <- data.frame(D = D.ex, D.str = c("Healthy", "Ill")[D.ex + 1], 
#                    M1 = M1, M2 = M2, stringsAsFactors = FALSE)
# head(test)
# longtest <- melt_roc(test, "D", c("M1", "M2"))
# head(longtest)
# ggplot(longtest, aes(d = D, m = M, color = name)) + geom_roc() + style_roc()


