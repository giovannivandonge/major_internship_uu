#!/bin/bash
mkdir -p ../netMHCpan_data/patient_subsets

while read STUDY_PATIENT; do
  # $0 = study, $1 = patient
  IFS=',' read -ra my_array <<< "$STUDY_PATIENT"
  STUDY="${my_array[0]}"
  PATIENT="${my_array[1]}"
  FILE_STUDY=$(echo "${my_array[0]}" | sed -r 's/\/ //g' | tr ' ' '_')
  FILE_PATIENT=$(echo "${my_array[1]}" | tr ' ' '_')

  # Select mutant or wildtype peptide from original data file with the combination
  # of study and patient
  egrep "$STUDY.*$PATIENT" < ../data/2017_Bjerregaard_subset.csv |
    awk -F ',' '{print $2}' > ../netMHCpan_data/patient_subsets/"$FILE_STUDY"-"$FILE_PATIENT"_mutant.txt
  egrep "$STUDY.*$PATIENT" < ../data/2017_Bjerregaard_subset.csv |
    awk -F ',' '{print $4}' > ../netMHCpan_data/patient_subsets/"$FILE_STUDY"-"$FILE_PATIENT"_wildtype.txt
done < ../netMHCpan_data/study_patient_list.txt
