#!/bin/bash

mkdir -p /home/giovanni/netMHCstab_data /home/giovanni/netMHCstab_output /home/giovanni/netMHCstab_parsed
rm -rf /home/giovanni/netMHCstab_data/* ||:  # empty dir if it contains files

# MAKE SEPERATE FILES FOR EACH HLA ALLELE WITH CORRESPONDING PEPTIDES
HLA_PEPTIDE=$(awk -F ',' '{print $4","$5","$6}' < /home/giovanni/netMHCpan_parsed/peptide_data.csv | tr -d '"' | tail -n +2 | sort -u)
for pHLA in $HLA_PEPTIDE; do
  arrpHLA=(${pHLA//,/ })
  HLA="${arrpHLA[0]}"
  MUT_PEPTIDE="${arrpHLA[1]}"
  WT_PEPTIDE="${arrpHLA[2]}"
  echo "${MUT_PEPTIDE}" >> /home/giovanni/netMHCstab_data/"${HLA}"_"${#MUT_PEPTIDE}"-mer_mut.txt
  echo "${WT_PEPTIDE}" >> /home/giovanni/netMHCstab_data/"${HLA}"_"${#WT_PEPTIDE}"-mer_wt.txt
done

# RUN NETMHCSTAB ON SERVER FOR EVERY FILE
for FILE_PATH in /home/giovanni/netMHCstab_data/*
do
  arrFILE=(${FILE_PATH//\// })
  FILE_NAME=(${arrFILE[-1]::-4})

  arrHLA_MER=(${arrFILE[-1]//_/ })
  HLA_ALLELE=(${arrHLA_MER[0]})

  arrMER=(${arrHLA_MER[1]//-/ })
  LENGTH=(${arrMER[0]})

  /tbb/local/ii/bin/netMHCstabpan-1.0 -f ${FILE_PATH} -a ${HLA_ALLELE} -p -l ${LENGTH} -xls -xlsfile /home/giovanni/netMHCstab_output/NetMHCstabpan_${FILE_NAME}.xls
done


# PARSE OUTPUT AND WRITE TO NEW DIRECTORY
echo Peptide,HLA,Stab_pred,Stab_half_life,Stab_rank > /home/giovanni/netMHCstab_parsed/stab_mut_output_parsed.csv
echo Peptide,HLA,Stab_pred,Stab_half_life,Stab_rank > /home/giovanni/netMHCstab_parsed/stab_wt_output_parsed.csv

for OUTPUT_PATH in /home/giovanni/netMHCstab_output/*
do
  arrOUTPUT=(${OUTPUT_PATH//\// })
  subArrOUTPUT=(${arrOUTPUT[-1]//_/ })
  PEP_ORG=${subArrOUTPUT[-1]::-4}
  HLA_PARSED=(${subArrOUTPUT[1]})

  cat ${OUTPUT_PATH} | tail -n +3 | awk -F '\t' -v HLA=${HLA_PARSED} '{print $2","HLA","$4","$5","$6}' >> /home/giovanni/netMHCstab_parsed/stab_${PEP_ORG}_output_parsed.csv
done
