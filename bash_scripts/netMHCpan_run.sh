#!/bin/bash
for INPUT_FILE in ../netMHCpan_data/patient_subsets/*.txt; do
  # HLA file
  HLA_PATH_STRING=$(echo "${INPUT_FILE%_*}"_hla.txt)
  IFS='/' read -ra HLA_PATH <<< "$HLA_PATH_STRING"
  HLA_FILE_PATH=$(echo ../netMHCpan_data/patient_hla/"${HLA_PATH[-1]}")
  HLA_STRING=$(cat "${HLA_FILE_PATH}")

  # Output file
  IFS='/' read -ra END_FILE <<< "$INPUT_FILE"
  OUTPUT_FILE_EL=$(echo ../netMHCpan_output/ElutedLigand/EL_"${END_FILE[-1]%.*}".xls)
  OUTPUT_FILE_BA=$(echo ../netMHCpan_output/BindingAffinity/BA_"${END_FILE[-1]%.*}".xls)

  # Run EL
  ../../../tbb/local/ii/bin/netMHCpan-4.0 -f "$INPUT_FILE" -a "$HLA_STRING" -p -xls -xlsfile "$OUTPUT_FILE_EL" -inptype 1
  # Run BA
  ../../../tbb/local/ii/bin/netMHCpan-4.0 -BA -f "$INPUT_FILE" -a "$HLA_STRING" -p -xls -xlsfile "$OUTPUT_FILE_BA" -inptype 1
done

#../../tbb/local/ii/bin/netMHCpan-4.0
#-f netMHCpan_data/patient_subsets/Bassani-Sternberg_et_al_MEL15_mutant.txt
#-a netMHCpan_data/patient_hla/test_hla.txt
#-p
#-xls
#-xlsfile test_run.xls
#-inptype 1
